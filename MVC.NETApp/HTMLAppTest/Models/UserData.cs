﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTMLAppTest.Models
{
    public class UserData
    {
        public string UserName { get; set; }
        public string RequestedDate { get; set; }
        public string ServerDate { get; set; }
    }
}