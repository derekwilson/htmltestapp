﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTMLAppTest.Models;

namespace HTMLAppTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "HTML Test App";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public JsonResult GetMessage(UserData myData)
        {
            myData.UserName += " (verified)";
            var dt = DateTime.Now.AddYears(-1);
            DateTime.TryParse(myData.RequestedDate, out dt);
            myData.RequestedDate = dt.ToLongDateString();
            myData.ServerDate = DateTime.Now.ToShortDateString();

            // example 
            // {"UserName":"derek3 (verified)","RequestedDate":"Thursday, 2 August 2012","ServerDate":"6/09/2012"}
            
            return Json(myData, JsonRequestBehavior.AllowGet);
        }
    }
}
